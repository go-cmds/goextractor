package main

import (
        "flag"
        "fmt"
        "os"
        "strings"

        //Main
        // "gitlab.com/go-cmds/goextractor"

        //UnZip Decompressor
        zip "gitlab.com/go-cmds/goextractor/decompressors/zip"

        //Tar Decompressor
        tar "gitlab.com/go-cmds/goextractor/decompressors/tar"

        //Gzip Decompressor
        gzip "gitlab.com/go-cmds/goextractor/decompressors/gzip"

        //ar Decompressor
        ar "gitlab.com/go-cmds/goextractor/decompressors/ar"

        //xz Decompressor
        xz "gitlab.com/go-cmds/goextractor/decompressors/xz"
)


func main() {

        var ver = "0.1"
        var version bool
        flag.BoolVar(&version, "version", false, "version output")
        flag.Parse()
        if version {
            fmt.Println(os.Args[0] + " version is " + ver)
            os.Exit(0)
        }

        //fmt.Println("len", len(os.Args))

        if len(os.Args) < 3 {
        fmt.Println("Usage:", os.Args[0], "<FILE TO EXTRACT> <DESTINATION>")
        return
        }

        //for _, arg := range os.Args[1:] {
        //      fmt.Println(arg)
        //}

        fileName := os.Args[1]
        ss := strings.Split(fileName, ".")
        ext := ss[len(ss)-1]
        //fmt.Println(ext)

        destination := strings.Replace(os.Args[2], "../", "./", -1)

        if ext == "gz" || ext == "bz2" || ext == "lzma" {
         ext2 := ss[len(ss)-2]
         ext = ext2+"."+ext
        }

        if fileName == ext {
          fmt.Println("Please pass a file with an extension")
          return
        }

        fmt.Println("Extracting:",fileName,"with an extension of",ext)

        if ext == "zip" {
          //fmt.Println("Extracting Zip File")

          fmt.Println("UnZipping "+fileName+" to "+destination)
          err := zip.Unzip(fileName, destination)
          if err != nil {
            fmt.Println(err)
            os.Exit(1)
          }
         os.Exit(0)
        }

        if ext == "tar" {

          fmt.Println("UnTaring "+fileName+" to "+destination)
          err := tar.Untar(fileName, destination)
          if err != nil {
            fmt.Println(err)
            os.Exit(1)
          }
         os.Exit(0)
        }


        if ext == "tar.gz" || ext == "tgz" {

          fmt.Println("UnGziping "+fileName+" to "+destination+".tar")
          err := gzip.UnGzip(fileName, destination+".tar")
          if err != nil {
            fmt.Println(err)
            os.Exit(1)
          }
          err2 := tar.Untar(destination+".tar", destination)
          if err2 != nil {
            fmt.Println(err2)
            os.Exit(1)
          }
         os.Exit(0)
        }

        if ext == "ar" {

          fmt.Println("UnARing "+fileName+" to "+destination)
          err := ar.Unar(fileName, destination)
          if err != nil {
            fmt.Println(err)
            os.Exit(1)
          }
         os.Exit(0)
        }



        if ext == "xz" {

          fmt.Println("UnXZing "+fileName+" to "+destination)
          err := xz.UnXZ(fileName, destination)
          if err != nil {
            fmt.Println(err)
            os.Exit(1)
          }
         os.Exit(0)
        }




fmt.Println("No decompressor for file or file does not exist")
os.Exit(1)
}
