package gzip

import (
        "io"
        "os"
        "fmt"
        "compress/gzip"
        "path/filepath"
)


func Gzip(source, target string) error {
    reader, err := os.Open(source)
    if err != nil {
        return err
    }
 
    os.MkdirAll(target, 0755)

    filename := filepath.Base(source)
    target = filepath.Join(target, fmt.Sprintf("%s.gz", filename))
    writer, err := os.Create(target)
    if err != nil {
        return err
    }
    defer writer.Close()
 
    archiver := gzip.NewWriter(writer)
    archiver.Name = filename
    defer archiver.Close()
 
    _, err = io.Copy(archiver, reader)
    return err
}
 
func UnGzip(source, target string) error {
    reader, err := os.Open(source)
    if err != nil {
        return err
    }
    defer reader.Close()

    //os.MkdirAll(target, 0755)
 
    archive, err := gzip.NewReader(reader)
    if err != nil {
        return err
    }
    defer archive.Close()
 
    target = filepath.Join(target, archive.Name)
    writer, err := os.Create(target)
    if err != nil {
        return err
    }
    defer writer.Close()
 
    _, err = io.Copy(writer, archive)
    return err
}
